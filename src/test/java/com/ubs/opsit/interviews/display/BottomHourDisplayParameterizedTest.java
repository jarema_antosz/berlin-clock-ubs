package com.ubs.opsit.interviews.display;

import com.ubs.opsit.interviews.displays.BottomHourDisplay;
import com.ubs.opsit.interviews.displays.TopHourDisplay;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.Parameterized;

import java.util.Arrays;

import static org.junit.Assert.assertEquals;

/**
 * Created by Jarek on 2015-08-12.
 *
 * so many possible combinations make it great to use parameterized test
 */

@RunWith(Parameterized.class)
public class BottomHourDisplayParameterizedTest {

    private Integer second;
    private String displayed;

    BottomHourDisplay display;

    @Before
    public void setup() {
        display = new BottomHourDisplay();
    }

    @Parameterized.Parameters(name= "{index}: displayTime({0})={1}")
    public static Iterable<Object[]> data() {
        return Arrays.asList(new Object[][]{
                        {0, "OOOO"},
                        {1, "ROOO"},
                        {3, "RRRO"},
                        {5, "OOOO"},
                        {10, "OOOO"},
                        {12, "RROO"},
                        {17, "RROO"},
                        {18, "RRRO"},
                        {19, "RRRR"},
                        {20, "OOOO"},
                        {21, "ROOO"},
                        {24, "RRRR"}
                }
        );
    }

    public BottomHourDisplayParameterizedTest(Integer second, String displayed) {
        this.second = second;
        this.displayed = displayed;

    }

    @Test
    public void displayTime_shouldReturnCorrectValidationResult(){

        String result = display.displayTime(second);

        assertEquals(displayed, result);

    }
}
