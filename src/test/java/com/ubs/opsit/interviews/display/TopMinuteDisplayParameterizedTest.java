package com.ubs.opsit.interviews.display;

import com.ubs.opsit.interviews.displays.BottomMinuteDisplay;
import com.ubs.opsit.interviews.displays.TopMinuteDisplay;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.Parameterized;

import java.util.Arrays;

import static org.junit.Assert.assertEquals;

/**
 * Created by Jarek on 2015-08-12.
 * <p/>
 * so many possible combinations make it great to use parameterized test
 */

@RunWith(Parameterized.class)
public class TopMinuteDisplayParameterizedTest {

    private Integer second;
    private String displayed;

    TopMinuteDisplay display;

    @Before
    public void setup() {
        display = new TopMinuteDisplay();
    }

    @Parameterized.Parameters(name = "{index}: displayTime({0})={1}")
    public static Iterable<Object[]> data() {
        return Arrays.asList(new Object[][]{
                        {0, "OOOOOOOOOOO"},
                        {1, "OOOOOOOOOOO"},
                        {3, "OOOOOOOOOOO"},
                        {5, "YOOOOOOOOOO"},
                        {10, "YYOOOOOOOOO"},
                        {12, "YYOOOOOOOOO"},
                        {17, "YYROOOOOOOO"},
                        {18, "YYROOOOOOOO"},
                        {19, "YYROOOOOOOO"},
                        {20, "YYRYOOOOOOO"},
                        {21, "YYRYOOOOOOO"},
                        {24, "YYRYOOOOOOO"},
                        {45, "YYRYYRYYROO"},
                        {59, "YYRYYRYYRYY"}
                }
        );
    }

    public TopMinuteDisplayParameterizedTest(Integer second, String displayed) {
        this.second = second;
        this.displayed = displayed;

    }

    @Test
    public void displayTime_shouldReturnCorrectValidationResult() {

        String result = display.displayTime(second);

        assertEquals(displayed, result);

    }
}
