package com.ubs.opsit.interviews.display;

import com.ubs.opsit.interviews.displays.EvenSecondDisplay;
import com.ubs.opsit.interviews.displays.Lamp;
import org.junit.Before;
import org.junit.Test;

import static org.junit.Assert.assertEquals;

/**
 * Created by Jarek on 2015-08-12.
 */
public class EvenSecondDisplayTest {

    EvenSecondDisplay display;

    @Before
    public void setup() {
        display = new EvenSecondDisplay();
    }

    @Test
    public void canCallDisplayTimeMethod() {
        String timeString = display.displayTime(0);
    }

    @Test
    public void displayTime_shouldDisplayYellowLetterForZero() {
        int zero = 0;
        String result = display.displayTime(zero);

        assertEquals("Should display Y for 0", Lamp.YELLOW.getColorLetter(), result);
    }

    @Test
    public void displayTime_shouldDisplayOffForOne() {
        int second = 1;
        String result = display.displayTime(second);

        assertEquals("Should display " + Lamp.OFF.getColorLetter() + " for 0", Lamp.OFF.getColorLetter(), result);
    }

    @Test
    public void displayTime_shouldDisplayYellowLetterForTen() {
        int second = 10;
        String result = display.displayTime(second);

        assertEquals("Should display " + Lamp.YELLOW.getColorLetter() + " for 10", Lamp.YELLOW.getColorLetter(), result);
    }
}
