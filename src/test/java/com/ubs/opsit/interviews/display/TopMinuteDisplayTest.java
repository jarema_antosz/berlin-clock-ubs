package com.ubs.opsit.interviews.display;

import com.ubs.opsit.interviews.displays.TopMinuteDisplay;
import org.junit.Before;
import org.junit.Test;

import static org.junit.Assert.assertEquals;

/**
 * Created by Jarek on 2015-08-12.
 */
public class TopMinuteDisplayTest {

    private TopMinuteDisplay display;

    @Before
    public void setup() {
        this.display = new TopMinuteDisplay();
    }

    @Test
    public void canCallDisplayTimeMethod() {
        String timeString = display.displayTime(0);
    }

    @Test
    public void displayTime_shouldDisplayOffFor_0() {
        int minute = 0;
        String result = display.displayTime(minute);

        assertEquals("Should display OOOOOOOOOOO for 0", "OOOOOOOOOOO", result);
    }

    @Test
    public void displayTime_shouldDisplay_YYYYYYYYYYY_For_59() {
        int minute = 59;
        String result = display.displayTime(minute);

        assertEquals("Should display YYRYYRYYRYY for 59", "YYRYYRYYRYY", result);
    }

    @Test
    public void displayTime_shouldDisplay_YYROOOOOOOO_For_17() {
        int minute = 17;
        String result = display.displayTime(minute);

        assertEquals("Should display YYROOOOOOOO for 17", "YYROOOOOOOO", result);
    }
}
