package com.ubs.opsit.interviews.display;

import com.ubs.opsit.interviews.displays.BottomMinuteDisplay;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.Parameterized;

import java.util.Arrays;

import static org.junit.Assert.assertEquals;

/**
 * Created by Jarek on 2015-08-12.
 * <p/>
 * so many possible combinations make it great to use parameterized test
 */

@RunWith(Parameterized.class)
public class BottomMinuteDisplayParameterizedTest {

    private Integer second;
    private String displayed;

    BottomMinuteDisplay display;

    @Before
    public void setup() {
        display = new BottomMinuteDisplay();
    }

    @Parameterized.Parameters(name = "{index}: displayTime({0})={1}")
    public static Iterable<Object[]> data() {
        return Arrays.asList(new Object[][]{
                        {0, "OOOO"},
                        {1, "YOOO"},
                        {3, "YYYO"},
                        {5, "OOOO"},
                        {10, "OOOO"},
                        {12, "YYOO"},
                        {17, "YYOO"},
                        {18, "YYYO"},
                        {19, "YYYY"},
                        {20, "OOOO"},
                        {21, "YOOO"},
                        {24, "YYYY"},
                        {59, "YYYY"}
                }
        );
    }

    public BottomMinuteDisplayParameterizedTest(Integer second, String displayed) {
        this.second = second;
        this.displayed = displayed;

    }

    @Test
    public void displayTime_shouldReturnCorrectValidationResult() {

        String result = display.displayTime(second);

        assertEquals(displayed, result);

    }
}
