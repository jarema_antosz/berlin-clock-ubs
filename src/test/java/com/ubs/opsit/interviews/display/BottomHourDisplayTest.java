package com.ubs.opsit.interviews.display;

import com.ubs.opsit.interviews.displays.BottomHourDisplay;
import org.junit.Before;
import org.junit.Test;

import static org.junit.Assert.assertEquals;

/**
 * Created by Jarek on 2015-08-12.
 */
public class BottomHourDisplayTest {

    private BottomHourDisplay display;

    @Before
    public void setup() {
        this.display = new BottomHourDisplay();
    }

    @Test
    public void canCallDisplayTimeMethod() {
        String timeString = display.displayTime(0);
    }

    @Test
    public void displayTime_shouldDisplayOffForMidnight() {
        int hour = 0;
        String result = display.displayTime(hour);

        assertEquals("Should display OOOO for 0", "OOOO", result);
    }

    @Test
    public void displayTime_shouldDisplay_R000_for_1am() {
        int hour = 1;
        String result = display.displayTime(hour);

        assertEquals("Should display ROOO for 1", "ROOO", result);
    }

    @Test
    public void displayTime_shouldDisplay_RRRO_for_13() {
        int hour = 13;
        String result = display.displayTime(hour);

        assertEquals("Should display RRRO for 13", "RRRO", result);
    }

    @Test
    public void displayTime_shouldDisplay_RRRO_for_23() {
        int hour = 23;
        String result = display.displayTime(hour);

        assertEquals("Should display RRRO for 13", "RRRO", result);
    }

    @Test
    public void displayTime_shouldDisplay_RRRR_for_24() {
        int hour = 24;
        String result = display.displayTime(hour);

        assertEquals("Should display RRRR for 24", "RRRR", result);
    }

}
