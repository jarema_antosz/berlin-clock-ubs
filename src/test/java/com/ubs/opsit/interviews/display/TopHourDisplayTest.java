package com.ubs.opsit.interviews.display;

import com.ubs.opsit.interviews.displays.TopHourDisplay;
import org.junit.Before;
import org.junit.Test;

import static org.junit.Assert.assertEquals;

/**
 * Created by Jarek on 2015-08-12.
 */
public class TopHourDisplayTest {

    private TopHourDisplay display;

    @Before
    public void setup() {
        this.display = new TopHourDisplay();
    }

    @Test
    public void canCallDisplayTimeMethod() {
        String timeString = display.displayTime(0);
    }

    @Test
    public void displayTime_shouldDisplayOffForMidnight() {
        int hour = 0;
        String result = display.displayTime(hour);

        assertEquals("Should display 0000 for 0", "OOOO", result);
    }

    @Test
    public void displayTime_shouldDisplay_RR00_for_13pm() {
        int hour = 13;
        String result = display.displayTime(hour);

        assertEquals("Should display RR00 for 13", "RROO", result);
    }

    @Test
    public void displayTime_shouldDisplay_RRRR_for_20pm() {
        int hour = 20;
        String result = display.displayTime(hour);

        assertEquals("Should display RRRR for 20", "RRRR", result);
    }


}
