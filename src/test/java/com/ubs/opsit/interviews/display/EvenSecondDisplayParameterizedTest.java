package com.ubs.opsit.interviews.display;

import com.ubs.opsit.interviews.displays.EvenSecondDisplay;
import com.ubs.opsit.interviews.displays.Lamp;
import com.ubs.opsit.interviews.util.TimeStringValidator;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.Parameterized;

import java.util.Arrays;

import static org.junit.Assert.assertEquals;

/**
 * Created by Jarek on 2015-08-11.
 *
 * so many possible combinations make it great to use parameterized test
 */

@RunWith(Parameterized.class)
public class EvenSecondDisplayParameterizedTest {

    private Integer second;
    private String displayed;

    EvenSecondDisplay display;

    @Before
    public void setup() {
        display = new EvenSecondDisplay();
    }

    @Parameterized.Parameters(name= "{index}: displayTime({0})={1}")
    public static Iterable<Object[]> data() {
        return Arrays.asList(new Object[][]{
                        {0, Lamp.YELLOW.getColorLetter()},
                        {1, Lamp.OFF.getColorLetter()},
                        {2, Lamp.YELLOW.getColorLetter()},
                        {3, Lamp.OFF.getColorLetter()},
                        {10, Lamp.YELLOW.getColorLetter()},
                        {11, Lamp.OFF.getColorLetter()},
                        {59, Lamp.OFF.getColorLetter()},
                        {56, Lamp.YELLOW.getColorLetter()}
                }
        );
    }

    public EvenSecondDisplayParameterizedTest(Integer second, String displayed) {
        this.second = second;
        this.displayed = displayed;

    }

    @Test
    public void displayTime_shouldReturnCorrectValidationResult(){

        String result = display.displayTime(second);

        assertEquals(displayed, result);

    }
}
