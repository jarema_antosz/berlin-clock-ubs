package com.ubs.opsit.interviews.display;

import com.ubs.opsit.interviews.displays.BottomMinuteDisplay;
import org.junit.Before;
import org.junit.Test;

import static org.junit.Assert.assertEquals;

/**
 * Created by Jarek on 2015-08-12.
 */
public class BottomMinuteDisplayTest {

    private BottomMinuteDisplay display;

    @Before
    public void setup() {
        this.display = new BottomMinuteDisplay();
    }

    @Test
    public void canCallDisplayTimeMethod() {
        String timeString = display.displayTime(0);
    }

    @Test
    public void displayTime_shouldDisplayOffFor_0() {
        int minute = 0;
        String result = display.displayTime(minute);

        assertEquals("Should display OOOO for 0", "OOOO", result);
    }

    @Test
    public void displayTime_shouldDisplay_Y000_for_1st_minute() {
        int minute = 1;
        String result = display.displayTime(minute);

        assertEquals("Should display YOOO for 1", "YOOO", result);
    }

    @Test
    public void displayTime_shouldDisplay_YY00_for_2nd_minute() {
        int minute = 2;
        String result = display.displayTime(minute);

        assertEquals("Should display YYOO for 1", "YYOO", result);
    }

    @Test
    public void displayTime_shouldDisplay_YYYY_for_59th_minute() {
        int minute = 59;
        String result = display.displayTime(minute);

        assertEquals("Should display YYYY for 1", "YYYY", result);
    }
}
