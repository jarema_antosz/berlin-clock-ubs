package com.ubs.opsit.interviews.display;

import com.ubs.opsit.interviews.displays.TopHourDisplay;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.Parameterized;

import java.util.Arrays;

import static org.junit.Assert.assertEquals;

/**
 * Created by Jarek on 2015-08-12.
 * <p/>
 * so many possible combinations make it great to use parameterized test
 */

@RunWith(Parameterized.class)
public class TopHourDisplayParameterizedTest {

    private Integer second;
    private String displayed;

    TopHourDisplay display;

    @Before
    public void setup() {
        display = new TopHourDisplay();
    }

    @Parameterized.Parameters(name = "{index}: displayTime({0})={1}")
    public static Iterable<Object[]> data() {
        return Arrays.asList(new Object[][]{
                        {0, "OOOO"},
                        {1, "OOOO"},
                        {5, "ROOO"},
                        {10, "RROO"},
                        {12, "RROO"},
                        {17, "RRRO"},
                        {20, "RRRR"},
                        {21, "RRRR"},
                        {24, "RRRR"}
                }
        );
    }

    public TopHourDisplayParameterizedTest(Integer second, String displayed) {
        this.second = second;
        this.displayed = displayed;

    }

    @Test
    public void displayTime_shouldReturnCorrectValidationResult() {

        String result = display.displayTime(second);

        assertEquals(displayed, result);

    }
}
