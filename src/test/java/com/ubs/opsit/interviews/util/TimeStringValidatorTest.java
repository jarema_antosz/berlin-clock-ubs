package com.ubs.opsit.interviews.util;

import static org.junit.Assert.assertEquals;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.Parameterized;

import java.util.Arrays;

/**
 * Created by Jarek on 2015-08-11.
 *
 * so many possible combinations make it great to use parameterized test
 */

@RunWith(Parameterized.class)
public class TimeStringValidatorTest {

    private String aTime;
    private boolean valid;

    @Parameterized.Parameters(name= "{index}: isValid({0})={1}")
    public static Iterable<Object[]> data() {
        return Arrays.asList(new Object[][]{
                        {"00:00:00", true},
                        {"13:17:01", true},
                        {"23:59:59", true},
                        {"24:00:00", true},
                        {"24:01:60", false},
                        {"25:00:00", false},
                        {"88:88:88", false},
                        {" ", false},
                        {"AAAAAAAAA", false},
                        {"   ", false}
                }
        );
    }

    public TimeStringValidatorTest(String aTime, boolean valid) {
        this.aTime = aTime;
        this.valid = valid;

    }

    @Test
    public void isValid_shouldReturnCorrectValidationResult(){

        boolean validationResult = TimeStringValidator.isValid(aTime);

        assertEquals(validationResult, valid);

    }
}
