package com.ubs.opsit.interviews;

import org.junit.Before;
import org.junit.Test;
import static java.lang.System.lineSeparator;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;

/**
 * Created by Jarek on 2015-08-11.
 */
public class TimeConverterImplTest {

    private TimeConverterImpl timeConverter;

    @Before
    public void setup() {
        this.timeConverter = new TimeConverterImpl();
    }


    //first validate passed arg
    @Test(expected = IllegalArgumentException.class)
    public void convertTime_shouldThrowExceptionOnNullArg() {
        String aTime = null;
        timeConverter.convertTime(aTime);
    }

    @Test(expected = IllegalArgumentException.class)
    public void convertTime_shouldThrowExceptionOnEmptyArg() {
        String aTime = "";
        timeConverter.convertTime(aTime);
    }

    @Test(expected = IllegalArgumentException.class)
    public void convertTime_shouldThrowExceptionOnTooShortArg() {
        String aTime = " ";
        timeConverter.convertTime(aTime);

        //after this test I created TimeStringValidator util class and tested it separately
    }

    @Test
    public void convertTime_shouldReturnStringOnCorrectArg() {

        String aTime = "00:00:00";
        String clockString = timeConverter.convertTime(aTime);

        assertNotNull(clockString);

    }

    @Test
    public void convertTime_shouldReturnCorrectStringOnMidnightArg() {

        String aTime = "00:00:00";
        String clockString = timeConverter.convertTime(aTime);

        String correctResult = "Y" + lineSeparator() +
                "OOOO" + lineSeparator() +
                "OOOO" + lineSeparator() +
                "OOOOOOOOOOO" +  lineSeparator() +
                "OOOO";

        assertEquals("Invalid clock String returned", correctResult, clockString);

        //after this test the real fun begins. Time to implement internal display classes

    }
}
