package com.ubs.opsit.interviews.displays;

/**
 * Created by Jarek on 2015-08-12.
 */
public class BottomMinuteDisplay extends MinuteDisplay {

    private static final int LAMP_AMOUNT = 4;

    @Override
    public int getLampAmount() {
        return LAMP_AMOUNT;
    }

    protected int getLampAmountToOn(int hours) {
        return hours % TOP_MINUTE_UNIT;
    }
}
