package com.ubs.opsit.interviews.displays;

/**
 * Created by Jarek on 2015-08-12.
 */
public class EvenSecondDisplay {
    public String displayTime(int i) {

        if (i % 2 == 0) {
            return Lamp.YELLOW.getColorLetter();
        } else {
            return Lamp.OFF.getColorLetter();
        }

    }
}
