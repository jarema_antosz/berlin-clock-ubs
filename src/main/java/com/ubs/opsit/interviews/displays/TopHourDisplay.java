package com.ubs.opsit.interviews.displays;

/**
 * Created by Jarek on 2015-08-12.
 */
public class TopHourDisplay extends HourDisplay {

    protected int getLampAmountToOn(int hours) {
        return hours / TOP_HOUR_UNIT;
    }

}
