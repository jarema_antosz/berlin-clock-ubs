package com.ubs.opsit.interviews.displays;

/**
 * Created by Jarek on 2015-08-12.
 */
public abstract class HourDisplay {
    protected static final int TOP_HOUR_UNIT = 5;
    private static final int LAMP_AMOUNT = 4;

    public String displayTime(int hours) {

        int onLamps = getLampAmountToOn(hours);

        StringBuilder builder = new StringBuilder();
        for (int i = 0; i < onLamps; i++) {
            builder.append(Lamp.RED.getColorLetter());
        }

        while (builder.length() < LAMP_AMOUNT) {
            builder.append(Lamp.OFF.getColorLetter());
        }

        return builder.toString();
    }

    protected abstract int getLampAmountToOn(int hours);
}
