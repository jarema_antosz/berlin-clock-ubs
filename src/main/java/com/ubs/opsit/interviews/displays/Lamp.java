package com.ubs.opsit.interviews.displays;

/**
 * Created by Jarek on 2015-08-12.
 */
public enum Lamp {

    OFF("O"),
    YELLOW("Y"),
    RED("R");

    public String getColorLetter() {
        return colorLetter;
    }

    private String colorLetter;

    Lamp(String colorLetter){
        this.colorLetter = colorLetter;
    }


}
