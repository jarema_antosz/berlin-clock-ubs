package com.ubs.opsit.interviews.displays;

/**
 * Created by Jarek on 2015-08-12.
 */
public class TopMinuteDisplay extends MinuteDisplay {

    private static final int LAMP_AMOUNT = 11;

    @Override
    public String displayTime(int minutes) {
        return super.displayTime(minutes).replace("YYY", "YYR");
    }

    @Override
    public int getLampAmount() {
        return LAMP_AMOUNT;
    }

    protected int getLampAmountToOn(int hours) {
        return hours / TOP_MINUTE_UNIT;
    }
}
