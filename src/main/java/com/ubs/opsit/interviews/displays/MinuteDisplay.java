package com.ubs.opsit.interviews.displays;

/**
 * Created by Jarek on 2015-08-12.
 */
public abstract class MinuteDisplay {

    protected static final int TOP_MINUTE_UNIT = 5;

    public String displayTime(int minutes) {
        int onLamps = getLampAmountToOn(minutes);

        StringBuilder builder = new StringBuilder();
        for (int i = 0; i < onLamps; i++) {
            builder.append(Lamp.YELLOW.getColorLetter());
        }

        while (builder.length() < getLampAmount()) {
            builder.append(Lamp.OFF.getColorLetter());
        }

        return builder.toString();
    }

    protected abstract int getLampAmount();

    protected abstract int getLampAmountToOn(int hours);
}
