package com.ubs.opsit.interviews.util;

import java.util.regex.Pattern;

/**
 * Created by Jarek on 2015-08-11.
 */
public class TimeStringValidator {

    private static final String TIME24HOURS_PATTERN =
            "([01]?[0-9]|2[0-4]):[0-5][0-9]:[0-5][0-9]";

    private static final Pattern PATTERN = Pattern.compile(TIME24HOURS_PATTERN);

    public static boolean isValid(String aTime) {
        return PATTERN.matcher(aTime).matches();
    }
}
