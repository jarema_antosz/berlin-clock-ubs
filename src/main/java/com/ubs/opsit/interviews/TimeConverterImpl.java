package com.ubs.opsit.interviews;

import com.ubs.opsit.interviews.displays.*;
import com.ubs.opsit.interviews.util.TimeStringValidator;

import java.util.ArrayList;
import java.util.List;

import static java.lang.System.lineSeparator;

/**
 * Created by Jarek on 2015-08-11.
 */
public class TimeConverterImpl implements TimeConverter {

    private static final String TIME_PART_SEPARATOR = ":";

    private static final EvenSecondDisplay evenSecondDisplay = new EvenSecondDisplay();
    private static final TopHourDisplay topHourDisplay = new TopHourDisplay();
    private static final BottomHourDisplay bottomHourDisplay = new BottomHourDisplay();
    private static final TopMinuteDisplay topMinuteDisplay = new TopMinuteDisplay();
    private static final BottomMinuteDisplay bottomMinuteDisplay = new BottomMinuteDisplay();

    public String convertTime(String aTime) {

        validateTimeArg(aTime);

        List<Integer> timeParts = getTimePartsList(aTime);

        int hours = timeParts.get(0);
        int minutes = timeParts.get(1);
        int seconds = timeParts.get(2);

        String evenSecond = evenSecondDisplay.displayTime(seconds);
        String topHours = topHourDisplay.displayTime(hours);
        String bottomHours = bottomHourDisplay.displayTime(hours);
        String topMinutes = topMinuteDisplay.displayTime(minutes);
        String bottomMinutes = bottomMinuteDisplay.displayTime(minutes);

        return createResultString(evenSecond, topHours, bottomHours, topMinutes, bottomMinutes);

    }

    private String createResultString(String evenSecond, String topHours, String bottomHours, String topMinutes, String bottomMinutes) {
        return evenSecond + lineSeparator() + topHours + lineSeparator() + bottomHours + lineSeparator() + topMinutes + lineSeparator() + bottomMinutes;
    }

    private List<Integer> getTimePartsList(String aTime) {
        List<Integer> timeParts = new ArrayList<Integer>();
        for (String timePart : aTime.split(TIME_PART_SEPARATOR)) {
            timeParts.add(Integer.parseInt(timePart));
        }
        return timeParts;
    }

    private void validateTimeArg(String aTime) {
        if (aTime == null || "".equals(aTime)) {
            throw new IllegalArgumentException("Null argument passed");
        }

        //with convertTime_shouldThrowExceptionOnTooShortArg there is a need to introduce some util class to validate aTime arg
        //then test this class separately
        boolean isValid = TimeStringValidator.isValid(aTime);

        if (!isValid) {
            throw new IllegalArgumentException("Invalid argument format");
        }


    }
}
